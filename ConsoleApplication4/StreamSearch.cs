﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
    public static class StreamSearch
    {
        private static readonly int _bufferSize = 10240;

        public static IEnumerable<int> IndicesOfBruteForce(Stream stream, IList<byte> pattern)
        {
            if (stream == null) { throw new ArgumentNullException(nameof(stream)); }
            if (pattern == null) { throw new ArgumentNullException(nameof(pattern)); }
            var patternLength = pattern.Count;
            if (patternLength == 0) { return new List<int>() { 0 }; }
            var streamLength = stream.Length;
            if (streamLength < patternLength) { return new List<int>(); }

            stream.Seek(0, SeekOrigin.Begin);
            var indices = new List<int>();
            var buffer = new byte[patternLength];
            for (int i = 0; i <= (streamLength - patternLength); i++)
            {
                stream.Read(buffer, 0, patternLength);

                var j = 0;
                while (j < patternLength)
                {
                    if (buffer[j] != pattern[j]) { break; }
                    j++;
                }
                if (patternLength == j)
                {
                    indices.Add(i);
                }
                stream.Seek(-(patternLength - 1), SeekOrigin.Current);
            }
            return indices;
        }

        public static IEnumerable<int> IndicesOfBruteForceToArray(Stream stream, IList<byte> pattern)
        {
            if (stream == null) { throw new ArgumentNullException(nameof(stream)); }
            if (pattern == null) { throw new ArgumentNullException(nameof(pattern)); }
            var patternLength = pattern.Count;
            if (patternLength == 0) { return new List<int>() { 0 }; }
            var streamLength = (int)stream.Length;
            if (streamLength < patternLength) { return new List<int>(); }

            byte[] buffer = null;
            if (stream is MemoryStream)
            {
                buffer = ((MemoryStream)stream).GetBuffer();
            }
            else
            {
                stream.Seek(0, SeekOrigin.Begin);
                buffer = new byte[streamLength];
                stream.Read(buffer, 0, streamLength);
            }

            var indices = new List<int>();
            for (int i = 0; i <= (streamLength - patternLength); i++)
            {
                var j = 0;
                while (j < patternLength)
                {
                    if (buffer[i + j] != pattern[j]) { break; }
                    j++;
                }
                if (patternLength == j)
                {
                    indices.Add(i);
                }
            }
            return indices;
        }

        public static IEnumerable<int> IndicesOfBmh(Stream stream, IList<byte> pattern)
        {
            if (stream == null) { throw new ArgumentNullException(nameof(stream)); }
            if (pattern == null) { throw new ArgumentNullException(nameof(pattern)); }
            var patternLength = pattern.Count;
            if (patternLength == 0) { return new List<int>() { 0 }; }
            var streamLength = (int)stream.Length;
            if (streamLength < patternLength) { return new List<int>(); }

            var indices = new List<int>();
            if (patternLength == 1)
            {
                stream.Seek(0, SeekOrigin.Begin);
                var readByte = 0;
                while (readByte != -1)
                {
                    readByte = stream.ReadByte();
                    if (readByte == pattern[0])
                    {
                        indices.Add((int)stream.Position - 1);
                    }
                }
                return indices;
            }

            var skipTable = new int[256];
            for (int k = 0; k < skipTable.Length; k++)
            {
                skipTable[k] = patternLength;
            }
            for (int k = 0; k < patternLength - 1; k++)
            {
                skipTable[pattern[k]] = patternLength - k - 1;
            }

            var i = 0;
            var max = streamLength - patternLength;
            var buffer = new byte[patternLength];
            stream.Seek(0, SeekOrigin.Begin);
            while (i <= max)
            {
                stream.Seek(i, SeekOrigin.Begin);
                stream.Read(buffer, 0, patternLength);

                var j = patternLength - 1;
                while (j >= 0)
                {
                    if (buffer[j] != pattern[j]) { break; }
                    j--;
                }
                if (j < 0)
                {
                    indices.Add(i);
                }
                if (i >= max) { break; }
                stream.Seek(-1, SeekOrigin.Current);
                i += skipTable[stream.ReadByte()];
            }
            return indices;
        }

        public static IEnumerable<int> IndicesOfBmhToArray(Stream stream, IList<byte> pattern)
        {
            if (stream == null) { throw new ArgumentNullException(nameof(stream)); }
            if (pattern == null) { throw new ArgumentNullException(nameof(pattern)); }
            var patternLength = pattern.Count;
            if (patternLength == 0) { return new List<int>() { 0 }; }
            var streamLength = (int)stream.Length;
            if (streamLength < patternLength) { return new List<int>(); }

            byte[] buffer = null;
            if (stream is MemoryStream)
            {
                buffer = ((MemoryStream)stream).GetBuffer();
            }
            else
            {
                stream.Seek(0, SeekOrigin.Begin);
                buffer = new byte[streamLength];
                stream.Read(buffer, 0, streamLength);
            }

            var indices = new List<int>();
            if (patternLength == 1)
            {
                for (int n = 0; n < streamLength; n++)
                {
                    if (buffer[n] == pattern[0])
                    {
                        indices.Add(n);
                    }
                }
                return indices;
            }

            var skipTable = new int[256];
            for (int k = 0; k < skipTable.Length; k++)
            {
                skipTable[k] = patternLength;
            }
            for (int k = 0; k < patternLength - 1; k++)
            {
                skipTable[pattern[k]] = patternLength - k - 1;
            }

            var i = 0;
            var max = streamLength - patternLength;
            var count = 0;
            while (i <= max)
            {
                count++;
                var j = patternLength - 1;
                while (j >= 0)
                {
                    if (buffer[i + j] != pattern[j]) { break; }
                    j--;
                }
                if (j < 0)
                {
                    indices.Add(i);
                }
                if (i >= max) { break; }
                i += skipTable[buffer[i + patternLength - 1]];
            }
            return indices;
        }

        public static IEnumerable<int> IndicesOfBmhSplitToArray(Stream stream, IList<byte> pattern)
        {
            if (stream == null) { throw new ArgumentNullException(nameof(stream)); }
            if (pattern == null) { throw new ArgumentNullException(nameof(pattern)); }
            var patternLength = pattern.Count;
            if (patternLength == 0) { return new List<int>() { 0 }; }
            var streamLength = (int)stream.Length;
            if (streamLength < patternLength) { return new List<int>(); }

            IList<int> skipTable = null;
            if (patternLength > 0)
            {
                skipTable = new int[256];
                for (int i = 0; i < skipTable.Count; i++)
                {
                    skipTable[i] = patternLength;
                }
                for (int i = 0; i < patternLength - 1; i++)
                {
                    skipTable[pattern[i]] = patternLength - i - 1;
                }
            }

            var pageSize = stream is MemoryStream ? 1 : (streamLength / _bufferSize) + ((streamLength % _bufferSize) > 0 ? 1 : 0);
            var indices = new List<int>();
            for (int pageIndex = 0; pageIndex < pageSize; pageIndex++)
            {
                var offset = (_bufferSize * pageIndex) - (pageIndex > 0 ? patternLength - 1 : 0);
                byte[] buffer = null;
                int bufferSize = 0;
                if (stream is MemoryStream)
                {
                    buffer = ((MemoryStream)stream).GetBuffer();
                    bufferSize = streamLength;
                }
                else
                {
                    stream.Seek(offset, SeekOrigin.Begin);
                    buffer = new byte[Math.Min(streamLength - offset, _bufferSize + (pageIndex > 0 ? patternLength - 1 : 0))];
                    bufferSize = buffer.Length;
                    stream.Read(buffer, 0, bufferSize);
                }

                if (patternLength == 1)
                {
                    for (int n = 0; n < bufferSize; n++)
                    {
                        if (buffer[n] == pattern[0])
                        {
                            indices.Add(n + offset);
                        }
                    }
                    return indices;
                }

                var i = 0;
                var max = bufferSize - patternLength;
                while (i <= max)
                {
                    var j = patternLength - 1;
                    while (j >= 0)
                    {
                        if (buffer[i + j] != pattern[j]) { break; }
                        j--;
                    }
                    if (j < 0)
                    {
                        indices.Add(i + offset);
                    }
                    if (i >= max) { break; }
                    i += skipTable[buffer[i + patternLength - 1]];
                }
            }
            return indices;
        }

        public static IEnumerable<int> IndicesOfQuickSearch(Stream stream, IList<byte> pattern)
        {
            if (stream == null) { throw new ArgumentNullException(nameof(stream)); }
            if (pattern == null) { throw new ArgumentNullException(nameof(pattern)); }
            var patternLength = pattern.Count;
            if (patternLength == 0) { return new List<int>() { 0 }; }
            var streamLength = (int)stream.Length;
            if (streamLength < patternLength) { return new List<int>(); }

            var indices = new List<int>();
            if (patternLength == 1)
            {
                stream.Seek(0, SeekOrigin.Begin);
                var readByte = 0;
                while (readByte != -1)
                {
                    readByte = stream.ReadByte();
                    if (readByte == pattern[0])
                    {
                        indices.Add((int)stream.Position - 1);
                    }
                }
                return indices;
            }

            var skipTable = new int[256];
            for (int k = 0; k < skipTable.Length; k++)
            {
                skipTable[k] = patternLength + 1;
            }
            for (int k = 0; k < patternLength; k++)
            {
                skipTable[pattern[k]] = patternLength - k;
            }

            var i = 0;
            var max = streamLength - patternLength;
            var buffer = new byte[patternLength];
            stream.Seek(0, SeekOrigin.Begin);
            while (i <= max)
            {
                stream.Seek(i, SeekOrigin.Begin);
                stream.Read(buffer, 0, patternLength);

                var j = 0;
                while (j < patternLength)
                {
                    if (buffer[j] != pattern[j]) { break; }
                    j++;
                }
                if (patternLength == j)
                {
                    indices.Add(i);
                }
                if (i >= max) { break; }
                i += skipTable[stream.ReadByte()];
            }
            return indices;
        }

        public static IEnumerable<int> IndicesOfQuickSearchToArray(Stream stream, IList<byte> pattern)
        {
            if (stream == null) { throw new ArgumentNullException(nameof(stream)); }
            if (pattern == null) { throw new ArgumentNullException(nameof(pattern)); }
            var patternLength = pattern.Count;
            if (patternLength == 0) { return new List<int>() { 0 }; }
            var streamLength = (int)stream.Length;
            if (streamLength < patternLength) { return new List<int>(); }

            byte[] buffer = null;
            if (stream is MemoryStream)
            {
                buffer = ((MemoryStream)stream).GetBuffer();
            }
            else
            {
                stream.Seek(0, SeekOrigin.Begin);
                buffer = new byte[streamLength];
                stream.Read(buffer, 0, streamLength);
            }

            var indices = new List<int>();
            if (patternLength == 1)
            {
                for (int n = 0; n < streamLength; n++)
                {
                    if (buffer[n] == pattern[0])
                    {
                        indices.Add(n);
                    }
                }
                return indices;
            }

            var skipTable = new int[256];
            for (int k = 0; k < skipTable.Length; k++)
            {
                skipTable[k] = patternLength + 1;
            }
            for (int k = 0; k < patternLength; k++)
            {
                skipTable[pattern[k]] = patternLength - k;
            }

            var i = 0;
            var max = streamLength - patternLength;
            while (i <= max)
            {
                var j = 0;
                while (j < patternLength)
                {
                    if (buffer[i + j] != pattern[j]) { break; }
                    j++;
                }
                if (patternLength == j)
                {
                    indices.Add(i);
                }
                if (i >= max) { break; }
                i += skipTable[buffer[i + patternLength]];
            }
            return indices;
        }

        public static IEnumerable<int> IndicesOfQuickSearchSplitToArray(Stream stream, IList<byte> pattern)
        {
            if (stream == null) { throw new ArgumentNullException(nameof(stream)); }
            if (pattern == null) { throw new ArgumentNullException(nameof(pattern)); }
            var patternLength = pattern.Count;
            if (patternLength == 0) { return new List<int>() { 0 }; }
            var streamLength = (int)stream.Length;
            if (streamLength < patternLength) { return new List<int>(); }

            IList<int> skipTable = null;
            if (patternLength > 0)
            {
                skipTable = new int[256];
                for (int i = 0; i < skipTable.Count; i++)
                {
                    skipTable[i] = patternLength + 1;
                }
                for (int i = 0; i < patternLength; i++)
                {
                    skipTable[pattern[i]] = patternLength - i;
                }
            }

            var pageSize = stream is MemoryStream ? 1 : (streamLength / _bufferSize) + ((streamLength % _bufferSize) > 0 ? 1 : 0);
            var indices = new List<int>();
            for (int pageIndex = 0; pageIndex < pageSize; pageIndex++)
            {
                var offset = (_bufferSize * pageIndex) - (pageIndex > 0 ? patternLength - 1 : 0);
                byte[] buffer = null;
                int bufferSize = 0;
                if (stream is MemoryStream)
                {
                    buffer = ((MemoryStream)stream).GetBuffer();
                    bufferSize = streamLength;
                }
                else
                {
                    stream.Seek(offset, SeekOrigin.Begin);
                    buffer = new byte[Math.Min(streamLength - offset, _bufferSize + (pageIndex > 0 ? patternLength - 1 : 0))];
                    bufferSize = buffer.Length;
                    stream.Read(buffer, 0, bufferSize);
                }

                if (patternLength == 1)
                {
                    for (int n = 0; n < bufferSize; n++)
                    {
                        if (buffer[n] == pattern[0])
                        {
                            indices.Add(n + offset);
                        }
                    }
                    return indices;
                }

                var i = 0;
                var max = bufferSize - patternLength;
                while (i <= max)
                {
                    var j = 0;
                    while (j < patternLength)
                    {
                        if (buffer[i + j] != pattern[j]) { break; }
                        j++;
                    }
                    if (patternLength == j)
                    {
                        indices.Add(i + offset);
                    }
                    if (i >= max) { break; }
                    i += skipTable[buffer[i + patternLength]];
                }
            }
            return indices;
        }

        public static IEnumerable<int> IndicesOfToString(Stream stream, IList<byte> pattern)
        {
            if (stream == null) { throw new ArgumentNullException(nameof(stream)); }
            if (pattern == null) { throw new ArgumentNullException(nameof(pattern)); }
            var patternLength = pattern.Count;
            if (patternLength == 0) { return new List<int>() { 0 }; }
            var streamLength = (int)stream.Length;
            if (streamLength < patternLength) { return new List<int>(); }

            stream.Seek(0, SeekOrigin.Begin);
            var buffer = new byte[streamLength];
            stream.Read(buffer, 0, streamLength);
            var text = Encoding.ASCII.GetString(buffer);
            var patternText = Encoding.ASCII.GetString(pattern.ToArray());

            var indices = new List<int>();
            var i = 0;
            while (true)
            {
                i = text.IndexOf(patternText, i, StringComparison.Ordinal);
                if (i < 0)
                {
                    return indices;
                }
                indices.Add(i);
                i++;
            }
        }
    }
}
