﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
    class Program
    {
        static void Main(string[] args)
        {
            var test = new StreamTest();
            test.Do();
            Console.ReadLine();
        }

        class StreamTest
        {
            private IList<byte> _pattern;
            private Stream _stream;

            public StreamTest()
            {
                _pattern = Encoding.UTF8.GetBytes("--57224ab5-b144-45b5-9f3b-bb2e81327a15");
                //_pattern = Encoding.UTF8.GetBytes("--57224ab5-b144-45b5-9f3b-bb2e81327a15--\r\n");
                //_pattern = Encoding.UTF8.GetBytes("---");
                _stream = File.OpenRead(Path.Combine(Environment.CurrentDirectory, "hoge.dat"));

                var ms = new MemoryStream();
                _stream.Seek(0, SeekOrigin.Begin);
                _stream.CopyTo(ms);
                _stream = ms;
            }

            public void Do()
            {
                for (int i = 0; i < 1; i++)
                {
                    DoTest(StreamSearch.IndicesOfBruteForce);
                    DoTest(StreamSearch.IndicesOfBruteForceToArray);
                    DoTest(StreamSearch.IndicesOfBmh);
                    DoTest(StreamSearch.IndicesOfBmhToArray);
                    DoTest(StreamSearch.IndicesOfBmhSplitToArray);
                    DoTest(StreamSearch.IndicesOfQuickSearch);
                    DoTest(StreamSearch.IndicesOfQuickSearchToArray);
                    DoTest(StreamSearch.IndicesOfQuickSearchSplitToArray);
                    DoTest(StreamSearch.IndicesOfToString);
                }
            }

            private delegate IEnumerable<int> IndicesOfDelegate(Stream stream, IList<byte> pattern);

            private void DoTest(IndicesOfDelegate method)
            {
                IEnumerable<int> result = null;
                var sw = new Stopwatch();
                sw.Start();
                result = method(_stream, _pattern);
                sw.Stop();
                Console.WriteLine("{0}:\n  Indices:{1}\n  Time:{2}\n", method.Method.Name, string.Join(",", result), sw.Elapsed);
                GC.Collect();
            }
        }
    }
}
